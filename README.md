# DoGames Speedmodeling Challenge

This repository holds all the resources and models used for speedmodeling challenges. This was inspired by [Imphenzia's speedmodeling setup](https://www.youtube.com/watch?v=BlxiCd0Upg4).

`Template/speedmodeling.blend` contains the base template which can be downloaded to try this yourself!

### Speedmodeling setup
Official/Community add-ons (under Preferences > Add-ons):
- Mesh: F2 (hit f to make a face when selecting a single vertex)
- Mesh: Auto-mirror (quickly setup mirroring)
- Mesh: LoopTools (for cutting holes)
- Object: Booltool (faster boolean operations)

##### Recommended add-ons (not needed for speedmodeling, but nice to have):
- Import-Export: Import Images as Planes
- [3D View: FBX Bundle](https://github.com/TiliSleepStealer/blender-addon-fbx-bundle)
- [Screencast keys](https://github.com/nutti/Screencast-Keys/releases)
- [UV: UV squares](https://github.com/Radivarig/UvSquares)
- [Blender-to-Unity exporter](https://github.com/EdyJ/blender-to-unity-fbx-exporter.git) Use this instead of FBX: Bundle for exporting animations.